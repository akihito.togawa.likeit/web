<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ユーザー削除</title>
    <!-- BootstrapのCSS読み込み -->
   <link href="css/bootstrap.min.css" rel="stylesheet">
   <!-- オリジナルCSS読み込み -->
   <link href="css/Login.css" rel="stylesheet">
   <!-- Jqeryの読み込み -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
   <!-- BootstrapのJS読み込み -->
   <script src="js/bootstrap.min.js"></script>
   <!-- レイアウトカスタマイズ用個別CSS -->
</head>

<body>

<!-- header -->
<header>
  <nav class="navbar navbar-inverse">
  	<div class="container">
  		<div class="navbar-header">
          <a class="navbar-brand" href="userCreate.html">ユーザ管理システム</a>
  		</div>

     	<ul class="nav navbar-nav navbar-right">
	        <li class="navbar-text">${userInfo.name} さん </li>
			<li class="dropdown">
			<a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
	    </li>
  	  </ul>
  	</div>
  </nav>
</header>

<!--    タイトル-->
<h1>ユーザー削除確認</h1>
	 <label for="input"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ログインID</font></font></label>
	 <p>${user.loginId}</p>
<br>
<p>を本当に削除してよろしいでしょうか。</p>
	<!-- ログインID -->
	<input type="hidden" name="login_id" value="${user.loginId}">

	<!-- パスワード -->
	<input type="hidden" name="password" value="${user.password}">

	<!-- 生年月日 -->
	<input type="hidden" name="password" value="${user.birthDate}">

	<!--　キャンセルボタン-->
	<form  action="UserListServlet" method="get">
		<button type="submit"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">キャンセル</font></font></button>
	</form>

	<!--OK-->
	<form action="UserDeleteServlet" method="post">
		<!-- ID -->
		<input type="hidden" name="id" value="${user.id}">
		<button type="submit"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OK</font></font></button>
	</form>
</body>
</html>