<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ログイン画面</title>
    <!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- オリジナルCSS読み込み -->
    <link href="css/Login.css" rel="stylesheet">
    <!-- Jqeryの読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    <!-- BootstrapのJS読み込み -->
    <script src="js/bootstrap.min.js"></script>
    <!-- レイアウトカスタマイズ用個別CSS -->
</head>
<body>
<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

<h1>ログイン画面</h1>
 <form  action="LoginServlet" method="post">
	 <div class="row">
	   <div class="col-sm-12">
	     <p>
           <label for="input"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ログインID</font></font></label>
           <input type="text" name="loginId" id="inputLoginId" class="form-control" placeholder="ログインID" required autofocus>
         </p>
	   </div>
	 </div>

	<div class="row">
	  <div class="col-sm-12">
	    <p>
          <label for="input"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">パスワード</font></font></label>
           <input type="password" name="password" id="inputPassword" class="form-control" placeholder="パスワード" required>
        </p>
	  </div>
	</div>
	<button type="submit"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ログイン</font></font></button>
	</form>
</body>
</html>