<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ユーザー詳細</title>
    <!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- オリジナルCSS読み込み -->
    <link href="css/Login.css" rel="stylesheet">
    <!-- Jqeryの読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    <!-- BootstrapのJS読み込み -->
    <script src="js/bootstrap.min.js"></script>
    <!-- レイアウトカスタマイズ用個別CSS -->
</head>
<body>

<!-- header -->
<header>
  <nav class="navbar navbar-inverse">
  	<div class="container">
  		<div class="navbar-header">
          <a class="navbar-brand" href="userCreate.html">ユーザ管理システム</a>
  		</div>

     	<ul class="nav navbar-nav navbar-right">
	        <li class="navbar-text">${userInfo.name} さん </li>
			<li class="dropdown">
			<a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
	    </li>
  	  </ul>
  	</div>
  </nav>
</header>

<!--    タイトル-->
    <h1>ユーザー情報参照</h1>

<!--　　情報-->
    <table class="table table-striped">
               <thead>
                 <tr>
                   <th>ログインID</th>
                   <td>${user.loginId}</td>
                   <th></th>
                 </tr>
                 <tr>
                   <th>ユーザー名</th>
                   <td>${user.name}</td>
                   <th></th>
                 </tr>
                  <tr>
                   <th>生年月日</th>
                   <td>${user.birthDate}</td>
                   <th></th>
                 </tr>
                  <tr>
                   <th>登録日時</th>
                   <td>${user.createDate}</td>
                   <th></th>
                    <tr>
                   <th>更新日</th>
                   <td>${user.updateDate}</td>
                   <th></th>
                 </tr>
               </thead>
             </table>
    <!--    戻る-->
    <a href="UserListServlet">戻る</a>
</body>