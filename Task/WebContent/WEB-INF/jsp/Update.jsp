<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ユーザー情報更新</title>
    <!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- オリジナルCSS読み込み -->
    <link href="css/Login.css" rel="stylesheet">
    <!-- Jqeryの読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    <!-- BootstrapのJS読み込み -->
    <script src="js/bootstrap.min.js"></script>
    <!-- レイアウトカスタマイズ用個別CSS -->
</head>
<body>

<!-- header -->
<header>
  <nav class="navbar navbar-inverse">
  	<div class="container">
  		<div class="navbar-header">
        <a class="navbar-brand" href="userCreate.html">ユーザ管理システム</a>
  		</div>

      <ul class="nav navbar-nav navbar-right">
        <li class="navbar-text">${userInfo.name} さん </li>
	<li class="dropdown">
	  <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
        </li>
  </ul>
  	</div>
  </nav>
</header>

<!--    タイトル-->
<h1>ユーザー情報更新</h1>

<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

<!--    ログインID-->
   <form  action="UserUpdateServlet" method="post">

	<!-- ID -->
	 <input type="hidden" name="id" value="${user.id}">

	 <div class="row">
		<div class="col-sm-12">
			<p>
                <label for="input"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ログインID</font></font></label>
                 <div>${user.loginId}</div>
		</div>
	 </div>
<!--    パスワード-->
	 <div class="row">
		<div class="col-sm-12">
			<p>
               <label for="input"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">パスワード</font></font></label>
               <input type="password" name="updatePassword" id="inputPassword" class="form-control" placeholder="パスワード">
            </p>
		</div>
	</div>
<!--    パスワード（確認）-->
	 <div class="row">
		<div class="col-sm-12">
			<p>
               <label for="input"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">パスワード確認</font></font></label>
               <input type="password" name="updatePassword2" id="inputPassword" class="form-control" placeholder="パスワード確認">
            </p>
		</div>
	</div>
<!--    ユーザ名-->
	 <div class="row">
		<div class="col-sm-12">
			<p>
               <label for="input"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ユーザー名</font></font></label>
               <input type="text" name="updateUser" id="inputUser" class="form-control" placeholder="ユーザー名" value="${user.name}">
            </p>
		</div>
	</div>
<!--    生年月日-->
	 <div class="row">
		<div class="col-sm-12">
			<p>
               <label for="input"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">生年月日</font></font></label>
               <input type="text" name="updateBirthday" id="inputBirthday" class="form-control" placeholder="生年月日" value="${user.birthDate}">
            </p>
		</div>
	</div>
<!--　更新ボタン-->
	<button type="submit"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">更新</font></font></button>
	</form>
<!--    戻る-->
    <br>
    <a href="UserListServlet">戻る</a>
</body>