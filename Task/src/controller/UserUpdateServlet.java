package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UpdeteServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);


		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
			UserDao userDao =new UserDao();
			User user =userDao.detail(id);


			System.out.println(user.getLoginId());
			System.out.println(user.getName());
			System.out.println(user.getBirthDate());
			System.out.println(user.getCreateDate());
			System.out.println(user.getUpdateDate());

			// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
		    request.setAttribute("user", user);
		    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Update.jsp");
			dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			request.setCharacterEncoding("UTF-8");

			// リクエストパラメータの入力項目を取得
		    String id = request.getParameter("id");
			System.out.println(id);
			String updatePassword = request.getParameter("updatePassword");
			String updatePassword2 = request.getParameter("updatePassword2");
			String updateUser = request.getParameter("updateUser");
			String updateBirthday = request.getParameter("updateBirthday");

			// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
			UserDao userDao = new UserDao();

			if(updateUser.equals("") && updateBirthday.equals("")) {
				request.setAttribute("errMsg", "入力された内容は正しくありません。");

				//リダイレクトだとサーブレットが実行されるのでフォワードでNew.jspに飛ばす
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Update.jsp");
				dispatcher.forward(request, response);
				return;//←をつけることで処理が下に流れずに、フォワードに飛ばせる
			}else if(!updatePassword.equals(updatePassword2)){
				request.setAttribute("errMsg", "入力された内容は正しくありません。");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Update.jsp");
				dispatcher.forward(request, response);
				return;
	        }

			if(updatePassword.equals("") || updatePassword.equals("")) {
				userDao.update1(id,updateUser, updateBirthday);
			}else {
				userDao.update2(id, userDao.password(updatePassword), updateUser, updateBirthday);
			}

			// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("UserListServlet");

	}

}
