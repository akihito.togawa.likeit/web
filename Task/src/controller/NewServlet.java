package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

/**
 * Servlet implementation class NewServler
 */
@WebServlet("/NewServlet")
public class NewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/New.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String newLoginId = request.getParameter("newLoginId");
		String newPassword = request.getParameter("newPassword");
		String newPassword2 = request.getParameter("newPassword2");
		String user = request.getParameter("user");
		String birthday = request.getParameter("birthday");

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userDao = new UserDao();

		//↓String型を比較する場合はequals()メソッドを使う
		if(newLoginId.equals("") && user .equals("") && birthday.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			//リダイレクトだとサーブレットが実行されるのでフォワードでNew.jspに飛ばす
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/New.jsp");
			dispatcher.forward(request, response);
			return;//←をつけることで処理が下に流れずに、フォワードに飛ばせる
		}else if(!newPassword.equals(newPassword2)){
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/New.jsp");
			dispatcher.forward(request, response);
			return;
        }else if(userDao.loginId(newLoginId) !=null) {
        	request.setAttribute("errMsg", "入力された内容は正しくありません。");
        	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/New.jsp");
			dispatcher.forward(request, response);
			return;
        }


		//if文でエラー条件に当てはまらないか確認してから新規登録メソッドを実行する
		userDao.newDate(newLoginId,  userDao.password(newPassword), user, birthday);

		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("UserListServlet");
	}
}
