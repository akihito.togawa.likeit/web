package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao {

    /**
     * ログインIDとパスワードに紐づくユーザ情報を返す
     * @param birthday2
     * @param loginId
     * @param password
     * @return
     */

	 /**
     * 全てのユーザ情報を取得する
     * @return
     */
    public List<User> findAll() {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            // TODO: 未実装：管理者以外を取得するようSQLを変更する
            String sql = "SELECT * FROM user WHERE login_id NOT IN ('admin')";

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }

    //ユーザー検索 ログインID
    public List<User>  find(String loginid, String userName, String dateS, String dateE){
    	   Connection conn = null;
           List<User> userList = new ArrayList<User>();

           try {
               // データベースへ接続
               conn = DBManager.getConnection();

               // SELECT文を準備
               String sql = "SELECT * FROM user WHERE login_id NOT IN ('admin')";

               if(!loginid.equals("")) {
            	   sql+=" and login_id = '" + loginid + "'";
               }

               if(!userName.equals("")) {
            	   sql+=" and name LIKE '%" + userName + "%'";
               }

               if(!dateS.equals("") && !dateE.equals("")) {
            	   sql+=" and  birth_date >= '"+ dateS +"' and  birth_date <= '"+ dateE +"'";
               }

               PreparedStatement pStmt = conn.prepareStatement(sql);
               ResultSet rs = pStmt.executeQuery();

               // 結果表に格納されたレコードの内容を
               // Userインスタンスに設定し、ArrayListインスタンスに追加
               while (rs.next()) {
                   int id = rs.getInt("id");
                   String loginId = rs.getString("login_id");
                   String name = rs.getString("name");
                   Date birthDate = rs.getDate("birth_date");
                   String password = rs.getString("password");
                   String createDate = rs.getString("create_date");
                   String updateDate = rs.getString("update_date");
                   User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                   userList.add(user);
               }
           } catch (SQLException e) {
               e.printStackTrace();
               return null;
           } finally {
               // データベース切断
               if (conn != null) {
                   try {
                       conn.close();
                   } catch (SQLException e) {
                       e.printStackTrace();
                       return null;
                   }
               }
           }
           return userList;
    }

    //パスワード暗号化
    public String password(String password) {
    //ハッシュを生成したい元の文字列
    String source = password;
    //ハッシュ生成前にバイト配列に置き換える際のCharset
    Charset charset = StandardCharsets.UTF_8;
    //ハッシュアルゴリズム
    String algorithm = "MD5";

    //ハッシュ生成処理
    byte[] bytes = null;
	try {
		bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
	} catch (NoSuchAlgorithmException e) {
		// TODO 自動生成された catch ブロック
		e.printStackTrace();
	}
    String result = DatatypeConverter.printHexBinary(bytes);
    //標準出力
    System.out.println(result);
	return result;
    }

    //ログイン機能
	public User findByLoginInfo(String loginId, String password) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, password);
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new User(loginIdData, nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

	//新規登録
	public User newDate(String newLoginId, String newPassword, String user, String birthday) {
		 Connection conn = null;
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();


	            // SQL文を準備
	            String sql = "INSERT INTO user(login_id, name, birth_date, password, create_date, update_date) VALUES (?, ?, ?, ?, now(),now())";

	             // SELECTを実行し、結果表を取得
	            PreparedStatement pStmt = conn.prepareStatement(sql);
	            pStmt.setString(1, newLoginId);
	            pStmt.setString(2, user);
	            pStmt.setString(3, birthday);
	            pStmt.setString(4, newPassword);
	            pStmt.executeUpdate();

	        } catch (SQLException e) {
	            e.printStackTrace();
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
			return null;
	}

	//ログインID検索
	public String loginId(String newLoginId) {
        Connection conn = null;

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ?";

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            if (!rs.next()) {
                return null;
            }

            String loginId = rs.getString("login_id");
            System.out.println(loginId);

            return loginId;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }

    }


	//ユーザー情報詳細参照
    public User detail(String id) {
        Connection conn = null;

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            // TODO: 未実装：管理者以外を取得するようSQLを変更する
            String sql = "SELECT * FROM user WHERE id= ?";

            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, id);
            ResultSet rs = pStmt.executeQuery();

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            if (!rs.next()) {
                return null;
            }
                int Id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                return new User(Id, loginId, name, birthDate, password, createDate, updateDate);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

    //ユーザー情報更新
    public void update1(String id,String updateUser,String updateBirthday) {
    	Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // UPDATE文を準備
            String sql = "UPDATE user SET name = ?, birth_date = ? WHERE id = ?";

             // UPDATEを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, updateUser);
            pStmt.setString(2, updateBirthday);
            pStmt.setString(3, id);
            pStmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

	public void update2(String id, String updatePassword, String updateUser,String updateBirthday) {
		 Connection conn = null;
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // UPDATE文を準備
	            String sql = "UPDATE user SET password = ?, name = ?, birth_date = ? WHERE id = ?";

	             // UPDATEを実行し、結果表を取得
	            PreparedStatement pStmt = conn.prepareStatement(sql);
	            pStmt.setString(1, updatePassword);
	            pStmt.setString(2, updateUser);
	            pStmt.setString(3, updateBirthday);
	            pStmt.setString(4, id);
	            pStmt.executeUpdate();

	        } catch (SQLException e) {
	            e.printStackTrace();
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
	}

	//ユーザー情報削除
	public void delete(String id) {
		 Connection conn = null;
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // DELETE文を準備
	            String sql = "DELETE FROM user WHERE id = ?";

	             // DELETEを実行し、結果表を取得
	            PreparedStatement pStmt = conn.prepareStatement(sql);
	            pStmt.setString(1, id);
	            pStmt.executeUpdate();
	        } catch (SQLException e) {
	            e.printStackTrace();
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
	}
}

